﻿using System;
using UnityEngine;

namespace LUPI.Unity
{
    public class MinMaxAttribute : PropertyAttribute
    {
        public float

                Min,
                Max;

        public MinMaxAttribute(float min, float max)
        {
            Min = min;
            Max = max;
        }

        public MinMaxAttribute(int min, int max)
        {
            Min = min;
            Max = max;
        }
    }

    [Serializable]
    public struct MinMaxInt
    {
        public int

                Min,
                Max;

        public MinMaxInt(int min, int max)
        {
            Min = min;
            Max = max;
        }

        public float Clamp(float value)
        {
            return Mathf.Clamp((float) value, (float) Min, (float) Max);
        }
    }

    [Serializable]
    public struct MinMaxFloat
    {
        public float

                Min,
                Max;

        public MinMaxFloat(float min, float max)
        {
            Min = min;
            Max = max;
        }

        public float Clamp(float value)
        {
            return Mathf.Clamp((float) value, (float) Min, (float) Max);
        }
    }
}
