﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using LUPI.Editor.Attributes;

namespace LUPI.Editor.Attributes.Editor
{
    [CustomEditor(typeof(Component), editorForChildClasses: true)]
    public class EffectableEditor : UnityEditor.Editor
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();


            var mytype = serializedObject.targetObject.GetType();

            var fieldInfo = mytype.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

            foreach (var item in fieldInfo)
            {
                var l = item.GetCustomAttributes<SerializeProperty>(true).ToList();
                if (l.Count > 0)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(item.Name);
                    EditorGUI.BeginDisabledGroup(true);
                    EditorGUILayout.TextField(item.GetValue(serializedObject.targetObject).ToString());
                    EditorGUI.EndDisabledGroup();
                    EditorGUILayout.EndHorizontal();
                }
            }
            serializedObject.ApplyModifiedProperties();

        }

    }

}

