﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LUPI.Editor.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
    public sealed class SerializeProperty : PropertyAttribute
    {
        public SerializeProperty(){}
    }

}



