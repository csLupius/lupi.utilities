﻿using System;
using System.Collections.Generic;

namespace LUPI.Pooling
{
    public class Pool<T> : IPool<T>

    {
        public Pool(bool expandable = false, Func<T> generateMethod = null)
        {
            if (expandable && generateMethod == null) throw new NotImplementedException("When declaring a pool as 'Expandable', generateMethod argument must be filled"); 
            generateAction = generateMethod;
            poolElements = new List<T>();
            activeElements = new List<T>();
        }

        private Func<T> generateAction;

        private List<T> poolElements = new List<T>();
        private List<T> activeElements = new List<T>();


        public bool Add(T target)
        {
            if (poolElements == null) poolElements = new List<T>();

            if (poolElements.Contains(target)) return false;

            poolElements.Add(target);

            return true;

        }

        

        public bool Remove(T target)
        {
            if (poolElements == null) poolElements = new List<T>();
            if (poolElements.Count < 1) return false;

            if (!poolElements.Contains(target)) return false;

            poolElements.Remove(target);
            return true;

        }

     

        public T GetAvailable(bool expand)
        {
            if (poolElements.Count > 0)
            {
                var get = poolElements[poolElements.Count - 1];

                if (activeElements == null) activeElements = new List<T>();
                activeElements.Add(get);

                poolElements.Remove(get);

                return get;

            }
            else if (expand)
            {
                if (generateAction == null) return default(T);

                var get = generateAction.Invoke();
                activeElements.Add(get);
                return get;
            }
            else return default(T);

        }

        public bool Disable(T target)
        {
            if (activeElements.Count == 0) return false;
            if (!activeElements.Contains(target)) return false;
            poolElements.Add(target);
            activeElements.Remove(target);
            return true;
        }

        public void GenerateMethod(Func<T> action)
        {
            generateAction = action;
        }
    }
}
