﻿using System;
namespace LUPI.Pooling
{
    public interface IPool<T>
    {
        bool Add(T target);
        bool Remove(T target);

        T GetAvailable(bool expand);
        bool Disable(T target);



        void GenerateMethod(Func<T> action);
    }
}
