using System;
using System.Collections.Generic;
using System.Linq;

namespace LUPI.IOC
{
    public static class IoC
    {

        static Dictionary<Type, Func<object>> registrations = new Dictionary<Type, Func<object>>();
        public static void Register<TService, TImpl>() where TImpl : TService
        {
            registrations.Add(typeof(TService), () => GetInstance(typeof(TImpl)));
        }

        public static void Register<TService>(Func<TService> instanceCreator)
        {
            registrations.Add(typeof(TService), () => instanceCreator());
        }

        public static void Register<TService>(TService instance)
        {
            registrations.Add(typeof(TService), () => instance);
        }
        public static void UnRegister<TService>()
        {
            registrations.Remove(typeof(TService));
        }
        public static void RegisterSingleton<TService>(Func<TService> instanceCreator)
        {
            var lazy = new Lazy<TService>(instanceCreator);
            Register<TService>(() => lazy.Value);
        }

        public static T Resolve<T>()
        {
            return (T)GetInstance(typeof(T));
        }

        public static object GetInstance(Type serviceType)
        {
            Func<object> creator;
            if (registrations.TryGetValue(serviceType, out creator)) return creator();
            else if (!serviceType.IsAbstract) return CreateInstance(serviceType);
            else throw new InvalidOperationException("No registration for " + serviceType);
        }

        private static object CreateInstance(Type implementationType)
        {
            var ctor = implementationType.GetConstructors().Single();
            var parameterTypes = ctor.GetParameters().Select(p => p.ParameterType);
            var dependencies = parameterTypes.Select(t => GetInstance(t)).ToArray();
            return Activator.CreateInstance(implementationType, dependencies);
        }
    }
}