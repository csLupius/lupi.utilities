namespace LUPI.Singleton {

    public interface ISingleton<T> {
        T Instance { get; }
    }
}