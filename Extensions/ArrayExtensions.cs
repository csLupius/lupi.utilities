﻿using System;
using UnityEngine;

namespace LUPI.Extensions {
    public static class ArrayExtensions {
        public static void ForEach<T> (this T[] array, Action<T> action) {
            Array.ForEach<T> (array, action);
        }

        /// <summary>
        /// Returns minimun and maximum x,y values from Vector3 array;
        /// </summary>
        /// <param name="self"></param>
        /// <returns> 0 : MinimumX, 1 : MinimumY, 2 : MaximumX, 3 : MaximumY</returns>
        public static int[] GetExtremeXYValues (this Vector3[] self) {

            int[] values = new int[4];
            float min_x = self[0].x;
            float min_y = self[0].y;
            float max_x = self[0].x;
            float max_y = self[0].y;

            for (int i = 0; i < self.Length; i++) {

                if (min_x > self[i].x) min_x = self[i].x;
                if (min_y > self[i].y) min_y = self[i].y;
                if (max_x < self[i].x) max_x = self[i].x;
                if (max_y < self[i].y) max_y = self[i].y;

            }
            values[0] = (int) min_x;
            values[1] = (int) min_y;
            values[2] = (int) max_x;
            values[3] = (int) max_y;
            return values;
        }

    }
}