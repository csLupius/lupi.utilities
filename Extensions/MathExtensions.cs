﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace LUPI.Extensions
{

    public static class MathExtensions
    {
        /*
         x / 100 => F-32/180 => C?
         
        X - DS
        ---------
        KS - DS
         

        Celcius Aralığından 50 derece ise Fahrenait'te kaçtır
 
        50 - 0(Ds) / 100(Ks) - 0(Ds) = X(istenen) - 32(Ds) / 212(Ks)- 32(Ds)

        50/100 => X-32/180
        180*(50/100) = X-32
        180*(50/100) + 32 = X

        Bilinen - BilinenMin / BilinenMax - BilinenMin = istenen - istenenMin / istenenMax - istenenMin

         */
        public static double ConvertToCustomRange(this double self, double aMax, double aMin, double bMax, double bMin)
        {
            return (bMax - bMin) * ((self - aMin) / aMax) + (bMin);
        }

        public static int ConvertToCustomRange(this int self, int aMax, int aMin, int bMax, int bMin)
        {
            return (bMax - bMin) * ((self - aMin) / aMax) + (bMin);
        }
        public static float ConvertToCustomRange(this float self, float aMax, float aMin, float bMax, float bMin)
        {
            return (bMax - bMin) * ((self - aMin) / aMax) + (bMin);
        }


    }
}
