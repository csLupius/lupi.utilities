﻿using System;

namespace LUPI.Extensions {
    public static class TypeExtensions {
        public static bool Implements (this Type type, Type Interface) {
            // Debug.Log("target : " + Interface.ToString());
            // Debug.Log("base : " + type.ToString());
            return Interface.IsAssignableFrom (type);
        }

    }
}