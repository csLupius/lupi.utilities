using System.Collections.Generic;
using UnityEngine;

namespace LUPI.Extensions {
    public static class BoundsExtensions {

        public static Vector3[] GetCorners (this Bounds self) {

            List<Vector3> corners = new List<Vector3> ();

            corners.Add (new Vector3 (self.center.x + self.extents.x, self.center.y + self.extents.y, self.center.z + self.extents.y));

            corners.Add (new Vector3 (self.center.x + self.extents.x, self.center.y + self.extents.y, self.center.z - self.extents.y));

            corners.Add (new Vector3 (self.center.x + self.extents.x, self.center.y - self.extents.y, self.center.z + self.extents.y));

            corners.Add (new Vector3 (self.center.x + self.extents.x, self.center.y - self.extents.y, self.center.z - self.extents.y));

            /////////////////////////////////////////////////////
            /// 
            /// 

            corners.Add (new Vector3 (self.center.x - self.extents.x, self.center.y + self.extents.y, self.center.z + self.extents.y));

            corners.Add (new Vector3 (self.center.x - self.extents.x, self.center.y + self.extents.y, self.center.z - self.extents.y));

            corners.Add (new Vector3 (self.center.x - self.extents.x, self.center.y - self.extents.y, self.center.z + self.extents.y));

            corners.Add (new Vector3 (self.center.x - self.extents.x, self.center.y - self.extents.y, self.center.z - self.extents.y));

            return corners.ToArray ();
        }

    }
}